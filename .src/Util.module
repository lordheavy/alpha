' Gambas module file

' return the arch of a build command or a package file
Public Sub GetArch({File} As String) As String

  If (String.InStr({File}, "i686") > 0)
    Return "i686"
  Endif

  If (String.InStr({File}, "x86_64") > 0) Or (String.InStr({File}, "multilib") > 0)
    Return "x86_64"
  Endif

  ' default value (any)
  Return "x86_64"

End

' return the latest package
Public Sub GetLatestPackage(Package As String, Arch As String, Path As String) As String

  Dim $aResult As String[]

  $aResult = GetPackageList(Package, Arch, Path)
  $aResult = SortPackages($aResult)

  Return IIf(IsNull($aResult), "", $aResult[0])

End

' return list of packages
Public Sub GetPackageList(Package As String, Arch As String, Path As String) As String[]

  Dim $aTempResult, $aResult As New String[]

  $aTempResult = Dir(Path, Package & "-*-{" & Arch & ",any}.pkg.tar{,.xz,.zst}")

  If $aTempResult.Count > 0 Then
    For Each $sPackage As String In $aTempResult
      If (GetPackageName($sPackage) = Package) Then $aResult.Push($sPackage)
    Next
  Endif

  Return $aResult

End

' return the name of a package
Public Sub GetPackageName(Package As String) As String

  Dim $aResult As String[]

  $aResult = Split(Package, "-")
  $aResult.Remove($aResult.Count - 3, 3)

  Return $aResult.Join("-")

End

' return the extension of a package
Public Sub GetPackageExt(Package As String) As String

  Dim $aResult As String[]

  $aResult = Split(Package, "-")
  Return Mid($aResult.Last, InStr($aResult.Last, ".pkg."))

End

' return the version of a package with pkgrel
Public Sub GetPackageVersion(Package As String) As String

  Dim $aResult As String[]

  $aResult = Split(Package, "-")

  ' we return pkgver and pkgrel
  Return $aResult[$aResult.Count - 3] & "-" & $aResult[$aResult.Count - 2]

End

' return package list sorted - with epoch respected
Public Sub SortPackages(Packages As String[]) As String[]

  Dim $aEpoch As New String[]
  Dim $aNonEpoch As New String[]
  Dim $sPackage As String

  For Each $sPackage In Packages
    If (GetPackageVersion($sPackage) Like "?:*") Then
      $aEpoch.Add($sPackage)
    Else
      $aNonEpoch.Add($sPackage)
    Endif
  Next

  $aEpoch.Sort(gb.Descent + gb.Natural)
  $aNonEpoch.Sort(gb.Descent + gb.Natural)

  Return $aEpoch.insert($aNonEpoch)

End

' JSON beautifier - make it easy to read
Public Sub JSONformatter(JSONfile As JSONCollection) As String

  Dim $iString, $oString As String
  Dim $iStream As Stream

  Dim $sReadChar As String
  Dim $iTab As Integer
  Dim $bQuote, $bBracket As Boolean

  $iString = JSON.Encode(JSONfile)
  $iStream = Open String $iString For Read

  While (Not Eof($iStream))
    $sReadChar = Read #$iStream, 1
    If ($sReadChar = "{" And Not $bQuote) Then
      $iTab += 1
      $sReadChar &= "\n" & Space$($iTab * 2)
    Else If ($sReadChar = "}" And Not $bQuote) Then
      $iTab -= 1
      $sReadChar = "\n" & Space$($iTab * 2) & $sReadChar
    Else If ($sReadChar = "\"") Then
      $bQuote = Not $bQuote
    Else If ($sReadChar = "[") Then
      $bBracket = True
    Else If ($sReadChar = "]") Then
      $bBracket = False
    Else If ($sReadChar = ":" And Not $bQuote) Then
      $sReadChar &= " "
    Else If ($sReadChar = "," And Not $bQuote) Then
      If (Not $bBracket) Then
        $sReadChar &= "\n" & Space$($iTab * 2)
      Else
        $sReadChar &= " "
      Endif
    Endif

    $oString &= $sReadChar
  Wend

  Close $iStream
  ' replace null with ""
  $oString = Replace($oString, " null", " \"\"")

  Return $oString

End
